<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>


Команды для запуска

- cd database
- touch database.sqlite
- cd ../
- cp .env.example .env
- composer update
- npm install && npm run dev
- php artisan migrate --seed
- php artisan key:generate
- php artisan storage:link
- php artisan serve


