<?php

namespace App\Http\Controllers;

use App\Http\Requests\TagRequest;
use App\Http\Resources\TagResource;
use App\Models\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Illuminate\Support\Facades\Auth;

class TagController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Factory|View|Application
    {
        $tags = Auth::user()->tags;
        return view('tags.index', ['tags' => $tags]);

    }

    /**
     * @param Request $request
     * @return AnonymousResourceCollection
     */
    public function search(Request $request): AnonymousResourceCollection
    {
        $tags = Auth::user()->tags()->where('tag', 'LIKE', '%' . $request->get('search') . '%')->get();
        return TagResource::collection($tags);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view('tags.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TagRequest $request
     * @return RedirectResponse
     */
    public function store(TagRequest $request): RedirectResponse
    {
        $tag = Auth::user()->tags()->where('user_id', Auth::id())->where('tag', $request->get('tag'))->get();

        if (empty($tag)) {
            $tag = new Tag($request->all());
            $tag->user_id = Auth::id();
            $tag->save();
            return redirect()->route('tags.index')->with('success', 'Successfully created');
        } else {
            return redirect()->route('tags.index')->with('error', 'This tag is already exists!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Tag $tag
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(Tag $tag): View|Factory|Application
    {
        $this->authorize('update', $tag);
        return view('tags.edit', ['tag' => $tag]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TagRequest $request
     * @param Tag $tag
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(TagRequest $request, Tag $tag): RedirectResponse
    {
        $this->authorize('update', $tag);
        $tag->update($request->all());
        return redirect()->route('tags.index')->with('success', 'Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Tag $tag
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Tag $tag): RedirectResponse
    {
        $this->authorize('delete', $tag);
        $tag->delete();
        return redirect()->route('tags.index')->with('success', 'Successfully deleted');
    }
}
