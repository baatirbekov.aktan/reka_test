<?php

namespace App\Http\Controllers;

use App\Models\Task;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagTaskController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @param Task $task
     * @return Factory|View|Application
     */
    public function index(Task $task): View|Factory|Application
    {
        $tags = Auth::user()->tags;
        return view('tag_task.index', compact('tags', 'task'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $task = Task::findOrFail($request->get('task'));
        $task->tags()->syncWithoutDetaching($request->get('tag'));

        return redirect()->back()->with('success', 'Successfully created!');
    }


    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        $task = Task::findOrFail($request->get('task'));
        $task->tags()->detach($request->get('tag'));

        return redirect()->back()->with('success', 'Successfully deleted!');
    }

}
