<?php

namespace App\Http\Controllers;

use App\Http\Requests\TaskRequest;
use App\Models\Task;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TaskController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @param Task $task
     * @return View|Factory|Application
     */
    public function show(Task $task): View|Factory|Application
    {
        return view('tasks.show', compact('task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param TaskRequest $request
     * @return JsonResponse
     */
    public function store(TaskRequest $request): JsonResponse
    {
        $data = $this->getArr($request);
        $task = new Task($data);
        $task->user_id = Auth::id();
        $task->save();

        return response()->json(
            [
                'task' => view('tasks.task', ['task' => $task])->render(),
            ],
            201
        );

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Task $task
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function edit(Task $task): View|Factory|Application
    {
        $this->authorize('update', $task);
        return view('tasks.edit', ['task' => $task]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param TaskRequest $request
     * @param Task $task
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(TaskRequest $request, Task $task): RedirectResponse
    {
        $this->authorize('update', $task);
        $data = $this->getArr($request);
        $task->update($data);
        return redirect()->route('users.show', ['user' => Auth::user()])->with('success', 'Successfully added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Task $task
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Task $task): RedirectResponse
    {
        $this->authorize('update', $task);
        $task->delete();
        return redirect()->back()->with('success', 'Successfully deleted');
    }

    /**
     * @param TaskRequest|Request $request
     * @return array
     */
    public function getArr(TaskRequest|Request $request): array
    {
        $data = $request->all();
        $file = $request->file('picture');
        if (!is_null($file)) {

            $path = $file->store('pictures', 'public');
            $data['picture'] = $path;

            $image_name = md5($file->path()) . '.jpg';
            $resize = Image::make($file->path())->fit(150, 150)->encode('jpg');
            Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
            $data['preview'] = 'pictures/' . $image_name;
        }
        return $data;
    }
}
