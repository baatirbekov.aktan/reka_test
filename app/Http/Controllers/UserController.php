<?php

namespace App\Http\Controllers;

use App\Models\Task;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @return Factory|View|Application
     */
    public function index(): View|Factory|Application
    {
        $users = User::paginate(5);
        return view('users.index', compact('users'));
    }

    /**
     * @param User $user
     * @param Request $request
     * @return Application|Factory|View
     */
    public function show(User $user, Request $request): View|Factory|Application
    {
        $query = Task::where('user_id', $user->id);

        if ($request->has('task') && !empty($request->get('task'))) {
            $query->where('task', 'like', '%' . $request->get('task') . '%');
        }

        if ($request->has('tags')) {
            $query->whereHas('tags', function (Builder $query) use ($request) {
                $query->whereIn('tag', $request->get('tags'));
            });
        }

        if ($request->has('search') && !empty($request->get('search'))) {
            $query->whereHas('tags', function (Builder $query) use ($request) {
                $query->whereIn('tag', explode(' ', $request->get('search')));
            });
        }

        $tasks = $query->paginate(5)->appends(request()->query());


        return view('users.show', compact('user', 'tasks'));
    }
}
