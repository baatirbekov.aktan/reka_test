<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserFriendController extends Controller
{
    public function __construct()
    {
        return $this->middleware('auth');
    }

    /**
     * @param User $user
     * @return Response
     */
    public function addReadFriend(User $user): Response
    {

        Auth::user()->friends()->syncWithoutDetaching([$user->id =>  ['read' => true]]);

        return response()->noContent();
    }

    /**
     * @param User $user
     * @return Response
     */
    public function deleteReadFriend(User $user): Response
    {
        Auth::user()->friends()->syncWithoutDetaching([$user->id =>  ['read' => false]]);

        return response()->noContent();
    }

    /**
     * @param User $user
     * @return Response
     */
    public function addEditFriend(User $user): Response
    {
        Auth::user()->friends()->syncWithoutDetaching([$user->id =>  ['edit' => true]]);

        return response()->noContent();
    }

    /**
     * @param User $user
     * @return Response
     */
    public function deleteEditFriend(User $user): \Illuminate\Http\Response
    {
        Auth::user()->friends()->syncWithoutDetaching([$user->id =>  ['edit' => false]]);

        return response()->noContent();
    }
}
