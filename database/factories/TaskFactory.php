<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class TaskFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $rand = rand(1, 12);

        return [
            'task' => $this->faker->paragraph(2),
            'picture' => $this->getPicture($rand),
            'preview' => $this->getPreview($rand),
        ];
    }

    /**
     * @param $image_number
     * @return string
     */
    private function getPicture($image_number): string
    {
        $path = storage_path() . "/pictures/picture/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return 'pictures/' . $image_name;
    }

    /**
     * @param $image_number
     * @return string
     */
    private function getPreview($image_number): string
    {
        $path = storage_path() . "/pictures/picture/" . $image_number . ".jpg";
        $image_name = md5($path) . '.jpg';
        $resize = Image::make($path)->fit(150, 150)->encode('jpg');
        Storage::disk('public')->put('pictures/' . $image_name, $resize->__toString());
        return 'pictures/' . $image_name;
    }
}
