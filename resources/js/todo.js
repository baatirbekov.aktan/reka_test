$(() => {

    $(document).on('click', '#create_task_btn', function (e) {
        e.preventDefault();
        createTask(e);
    })

    const createTask = e => {
        const taskPicture = document.querySelector('#create_task_form input[name=picture]');
        const formData = new FormData();
        formData.append('task', $("#create_task_form input[name=task]").val());

        if (taskPicture.files[0] !== undefined) {
            formData.append('picture', taskPicture.files[0]);
        }

        $(`.create_task_errors`).text('');
        $(`#create_task_form input`).css('border', '1px solid #D1D5DB');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax({
            method: 'POST',
            url: `/tasks/`,
            data: formData,
            processData: false,
            contentType: false,
            dataType: 'json',
        }).done(successResponse => {
            $('#tasks_list').prepend(successResponse.task)
            $('#create_task_form').trigger('reset');
        }).fail(data => {

            $(`.create_task_errors`).text('');
            $(`#create_task_form input`).css('border', '1px solid #D1D5DB');

            $.each(data.responseJSON.errors, function (name, message) {
                console.log(message)
                $(`#create_task_form input[name="${name}"]`).css('border', '3px solid red');
                $(`#create_task_form div[class="${name} create_task_errors"]`).css('color', 'red').text(message[0]);
            });
        });
    };

    $('#search_tag').autocomplete({
        source: function (request, response) {
            let result = [];
            $.ajax({
                url: '/search/tag',
                type: 'GET',
                dataType: "json",
                data: {
                    search: request.term
                },
                success: function (data) {
                    $.each(data.data, function (index, value) {
                        result.push(value.tag);
                    });
                    response(result);
                }
            });
        },
        minLength: 3,
        search: true,
        select: function (event, ui) {
            const div = `<div class="input-group mb-3">
                            <span class="input-group-text">@</span>
                            <input name="tags[]" type="text" class="form-control" value="${ui.item.value}" readonly="">
                        </div>`;
            $('#tag_form').prepend(div);
            $(this).val('')
            return false;
        },
        autoFocus: true,
    });


    $(document).on('click', '#read_btn', function (e) {
        const element = e.currentTarget;
        const friend = $(element).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        if ($(element).is(":checked")) {

            $.ajax({
                method: 'GET',
                url: `/user_friend/read/${friend}`,
                processData: false,
                contentType: false,
                dataType: 'json',
            }).done(successResponse => {
            }).fail(data => {
            });
        } else {
            $.ajax({
                method: 'DELETE',
                url: `/user_friend/read/${friend}`,
                processData: false,
                contentType: false,
                dataType: 'json',
            }).done(successResponse => {
            }).fail(data => {
            });
        }
    });


    $(document).on('click', '#edit_btn', function (e) {
        const element = e.currentTarget;
        const friend = $(element).val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });


        if ($(element).is(":checked")) {

            $.ajax({
                method: 'GET',
                url: `/user_friend/edit/${friend}`,
                processData: false,
                contentType: false,
                dataType: 'json',
            }).done(successResponse => {
            }).fail(data => {
            });
        } else {
            $.ajax({
                method: 'DELETE',
                url: `/user_friend/edit/${friend}`,
                processData: false,
                contentType: false,
                dataType: 'json',
            }).done(successResponse => {
            }).fail(data => {
            });
        }
    });
})
