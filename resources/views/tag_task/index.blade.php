@extends('layouts.app')
@section('content')
    <h3>Create new tag <a class="btn btn-outline-primary" href="{{ route('tags.create') }}">Create</a></h3>
    <table class="table table-dark table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Tag</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody id="tasks_list">
        @foreach($tags as $tag)
            <tr>
                <td>{{ $tag->id }}</td>
                <td class="w-50">{{ $tag->tag }}</td>
                <td class="d-flex">
                    @if($task->tags()->where('tag_id', $tag->id)->count() > 0)
                        <form method="post" action="{{ route('tag_task.destroy') }}">
                            @csrf
                            @method('DELETE')
                            <input name="task" type="hidden" value="{{ $task->id }}">
                            <input name="tag" type="hidden" value="{{ $tag->id }}">
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    @else
                        <form method="post" action="{{ route('tag_task.store') }}">
                            @csrf
                            <input name="task" type="hidden" value="{{ $task->id }}">
                            <input name="tag" type="hidden" value="{{ $tag->id }}">
                            <button type="submit" class="btn btn-info mx-1">Add</button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <a href="{{ route('users.show', ['user' => Auth::user()]) }}" class="btn btn-outline-secondary">Back</a>
@endsection
