@extends('layouts.app')
@section('content')

    <form method="post" action="{{ route('tags.store') }}" enctype="multipart/form-data">
        @csrf
        <div class="mb-3">
            <label for="tag" class="form-label">Task</label>
            <input name="tag" value="{{ old('tag') }}" type="text"
                   class="form-control @if($errors->has('tag')) is-invalid @endif" id="tag">
            @error('tag')
            <p class="text-danger">{{$message}}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Create</button>
    </form>

@endsection

