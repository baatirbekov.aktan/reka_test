@extends('layouts.app')
@section('content')

    <form method="post" action="{{ route('tags.update', ['tag' => $tag]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="tag" class="form-label">Task</label>
            <input name="tag" value="{{ $tag->tag }}" type="text"
                   class="form-control @if($errors->has('tag')) is-invalid @endif" id="tag">
            @error('tag')
            <p class="text-danger">{{$message}}</p>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

@endsection
