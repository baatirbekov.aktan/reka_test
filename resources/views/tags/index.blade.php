@extends('layouts.app')
@section('content')

    <h3>Create new tag <a class="btn btn-outline-primary" href="{{ route('tags.create') }}">Create</a></h3>
    <table class="table table-dark table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Tag</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody id="tasks_list">
        @foreach($tags as $tag)
            @include('tags.tag', ['tag' => $tag])
        @endforeach
        </tbody>
    </table>
@endsection
