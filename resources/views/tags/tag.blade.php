<tr>
    <td>{{ $tag->id }}</td>
    <td class="w-50">{{ $tag->tag }}</td>
    <td class="d-flex">
        <form method="post" action="{{ route('tags.destroy', ['tag' => $tag]) }}">
            @csrf
            @method('DELETE')
            <button type="submit" class="btn btn-danger">Delete</button>
        </form>
        <a class="btn btn-outline-warning mx-1" href="{{route('tags.edit', ['tag' => $tag])}}">Edit</a>

    </td>
</tr>
