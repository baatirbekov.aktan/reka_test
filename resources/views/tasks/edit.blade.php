@extends('layouts.app')
@section('content')

    <form method="post" action="{{ route('tasks.update', ['task' => $task]) }}" enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="mb-3">
            <label for="task" class="form-label">Task</label>
            <input name="task" value="{{ $task->task }}" type="text"
                   class="form-control @if($errors->has('task')) is-invalid @endif" id="task">
            @error('task')
            <p class="text-danger">{{$message}}</p>
            @enderror


        </div>
        <div>
            <label for="picture" class="form-label">Picture</label>
            <input name="picture" class="form-control form-control-lg" id="picture" type="file">
        </div>
        <button type="submit" class="btn btn-primary">Update</button>
    </form>

@endsection
