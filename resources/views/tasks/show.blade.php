@extends('layouts.app')
@section('content')
    <div class="card">
        @if ($task->picture)
            <img src="{{ asset('/storage/' . $task->picture ) }}" class="card-img-top" alt="#">
        @endif
        <div class="card-body">
            <p class="card-text">{{ $task->task }}</p>
            <a href="#" class="btn btn-primary">Go back</a>
        </div>
    </div>
@endsection
