<tr>
    <td>{{ $task->id }}</td>
    <td class="w-50">{{ $task->task }}</td>

    @if($task->preview)
        <td><img src="{{ asset('/storage/' . $task->preview) }}" alt="#"></td>    </td>
    @else
        <td><img style="width: 150px; height: 150px"
                 src="https://thumbs.dreamstime.com/b/no-image-available-icon-photo-camera-flat-vector-illustration-132483141.jpg"
                 alt="#">
        </td>
    @endif


    <td>
        <div class="d-flex">
            @can('delete', $task)
                <form method="post" action="{{ route('tasks.destroy', ['task' => $task]) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-danger">Delete</button>
                </form>
            @endcan
            <a class="btn btn-outline-primary mx-1" href="{{route('tasks.show', ['task' => $task])}}">Show</a>
            @can('update', $task)
                <a class="btn btn-outline-warning" href="{{route('tasks.edit', ['task' => $task])}}">Edit</a>
                <a class="btn btn-outline-light mx-1" href="{{route('tag_task.index', ['task' => $task])}}">Add tag</a>
            @endcan
        </div>
    </td>
</tr>
