@extends('layouts.app')
@section('content')
    <h1>Users</h1>
    <table class="table table-dark table-striped">
        <thead>
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Permissions</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr>
                <td>{{$user->name}}</td>

                <td>
                    @if (\Illuminate\Support\Facades\Auth::id() != $user->id)
                        <div class="form-check">
                            <input
                                @if (Auth::user()->friends()->where('friend_id', $user->id)->where('read', true)->count() > 0)
                                    checked=""
                                @endif
                                class="form-check-input" type="checkbox" value="{{ $user->id }}" id="read_btn"
                                data-user_id="{{ $user->id }}">
                            <label class="form-check-label" for="read_btn">
                                read
                            </label>
                        </div>
                        <div class="form-check">
                            <input
                                @if (Auth::user()->friends()->where('friend_id', $user->id)->where('edit', true)->count() > 0)
                                    checked=""
                                @endif
                                class="form-check-input" type="checkbox" value="{{ $user->id }}" id="edit_btn"
                                data-user_id="{{ $user->id }}">
                            <label class="form-check-label" for="edit_btn">
                                edit
                            </label>
                        </div>
                    @endif
                </td>
                <td>{{ $user->email }}</td>
                <td>
                    @can('view', $user)
                        <a class="btn btn-outline-primary" href="{{route('users.show', ['user' => $user])}}">Show</a>
                    @endcan
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>

    <div class="d-flex justify-content-center p-5">
        {{ $users->links("pagination::bootstrap-4") }}
    </div>

@endsection
