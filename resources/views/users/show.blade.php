@extends('layouts.app')
@section('content')

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form id="create_task_form" method="post" action="{{ route('tasks.store') }}"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label for="task" class="form-label">Tasks</label>
                            <input name="task" value="{{ old('task') }}" type="text" class="form-control" id="task">
                            <div class="task create_task_errors">
                            </div>
                        </div>
                        <div>
                            <label for="picture" class="form-label">Picture</label>
                            <input name="picture" class="form-control form-control-lg" id="picture" type="file">
                            <div class="picture create_task_errors">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button id="create_task_btn" type="submit" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
    </div>



    @can('view', \Illuminate\Support\Facades\Auth::user())
        <div class="d-flex my-4 justify-content-between">
            <h3>{{$user->name}}
                <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                    Create task
                </button>
            </h3>
            <div>
                <form method="get" action="{{ route('users.show', ['user' => $user]) }}">
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="task">Search</span>
                        </div>
                        <input value="{{ Request::get('task') }}" name="task" type="text" class="form-control"
                               id="task">
                        <button class="btn btn-outline-secondary">Search</button>
                    </div>
                </form>
            </div>
        </div>
        <div class="d-flex my-4 justify-content-between">
            <div>
                <form id="tag_form" class="d-flex w-75" method="get"
                      action="{{ route('users.show', ['user' => $user]) }}">
                    <div class="input-group mb-3">
                        <button type="submit" class="btn btn-outline-secondary">Search</button>
                    </div>
                </form>
            </div>
            <div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="search">Tags</span>
                    </div>
                    <input name="task" type="text" class="form-control" id="search_tag">
                </div>
            </div>
        </div>
        <div class="my-4">
            <form id="tag_form" class="d-flex" method="get"
                  action="{{ route('users.show', ['user' => $user]) }}">
                <div class="input-group mb-3">
                    <span class="input-group-text" id="search">@</span>
                    <input value="{{ Request::get('search') }}" id="search" name="search" type="text" class="form-control" placeholder="Tags">
                </div>
                <div class="input-group mb-3">
                    <button type="submit" class="btn btn-outline-secondary">Search</button>
                </div>
            </form>
            <div id="search" class="form-text">Введите теги через пробел</div>
        </div>
    @else
        <div>
            <h3>{{$user->name}}</h3>
        </div>
    @endif





    <table class="table table-dark table-striped">
        <thead>
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Task</th>
            <th scope="col">Preview</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody id="tasks_list">
        @foreach($tasks as $task)
            @include('tasks.task', ['task' => $task])
        @endforeach
        </tbody>
    </table>
    <div class="d-flex justify-content-center p-5">
        {{ $tasks->links("pagination::bootstrap-4") }}
    </div>
@endsection
