<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();

Route::resource('users', \App\Http\Controllers\UserController::class)->only('index', 'show');

Route::resource('tasks', \App\Http\Controllers\TaskController::class)->except('create', 'index');

Route::resource('tags', \App\Http\Controllers\TagController::class)->except('show');
Route::get('/search/tag', [\App\Http\Controllers\TagController::class, 'search'])->name('tags.search');
Route::get('/tag_task/{task}', [\App\Http\Controllers\TagTaskController::class, 'index'])->name('tag_task.index');
Route::post('/tag_task', [\App\Http\Controllers\TagTaskController::class, 'store'])->name('tag_task.store');
Route::delete('/tag_task', [\App\Http\Controllers\TagTaskController::class, 'destroy'])->name('tag_task.destroy');


Route::get('/user_friend/read/{user}', [\App\Http\Controllers\UserFriendController::class, 'addReadFriend']);
Route::delete('/user_friend/read/{user}', [\App\Http\Controllers\UserFriendController::class, 'deleteReadFriend']);
Route::get('/user_friend/edit/{user}', [\App\Http\Controllers\UserFriendController::class, 'addEditFriend']);
Route::delete('/user_friend/edit/{user}', [\App\Http\Controllers\UserFriendController::class, 'deleteEditFriend']);
